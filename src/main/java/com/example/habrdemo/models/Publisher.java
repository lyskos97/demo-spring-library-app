package com.example.habrdemo.models;

import com.example.habrdemo.Logger;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.OneToMany;
import java.util.HashSet;
import java.util.Set;

@Entity
public class Publisher {
  @Id
  @GeneratedValue
  private Long id;
  private String name;
  private String address;

  @OneToMany
  private Set<Book> books = new HashSet<>();

  public Publisher() {
  }

  public Publisher(String name, String address) {
    this.name = name;
    this.address = address;

    Logger.getInstance().appendToLogs("New publisher: " + this.name);
  }

  public String getName() {
    return name;
  }

  public void setName(String name) {
    this.name = name;
  }

  public String getAddress() {
    return address;
  }

  public void setAddress(String address) {
    this.address = address;
  }
}
