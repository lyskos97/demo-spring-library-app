package com.example.habrdemo.models;

import com.example.habrdemo.Logger;

import javax.persistence.*;

@Entity
@Table(name = "books")
public class Book {
  @Id
  @GeneratedValue
  private Long id;
  private String title;
  private String isbn;

  @ManyToOne
  @JoinTable(name = "book_author",
          joinColumns = @JoinColumn(name = "book_id"),
          inverseJoinColumns = @JoinColumn(name = "author_id"))
  private Author author;

  @ManyToOne
  @JoinTable(name = "book_publisher",
          joinColumns = @JoinColumn(name = "book_id"),
          inverseJoinColumns = @JoinColumn(name = "publisher_id"))
  private Publisher publisher;

  public Book() {
  }

  public Book(String title, String isbn) {
    this.title = title;
    this.isbn = isbn;

    Logger.getInstance().appendToLogs("New book: " + this.title);
  }

  public String getTitle() {
    return title;
  }

  public void setTitle(String title) {
    this.title = title;
  }

  public String getIsbn() {
    return isbn;
  }

  public void setIsbn(String isbn) {
    this.isbn = isbn;
  }

  public Author getAuthor() {
    return author;
  }

  public void setAuthor(Author author) {
    this.author = author;
  }

  public Publisher getPublisher() {
    return publisher;
  }

  public void setPublisher(Publisher publisher) {
    this.publisher = publisher;
  }
}
