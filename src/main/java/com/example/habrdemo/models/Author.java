package com.example.habrdemo.models;

import com.example.habrdemo.Logger;

import javax.persistence.*;
import java.util.HashSet;
import java.util.Set;

@Entity
public class Author {
  @Id
  @GeneratedValue
  private Long id;
  private String firstName;
  private String lastName;

  @OneToMany
  private Set<Book> books = new HashSet<>();

  public Author() {
  }

  public Author(String firstName, String lastName) {
    this.firstName = firstName;
    this.lastName = lastName;

    Logger.getInstance().appendToLogs("New author: " + this.firstName + " " + this.lastName);
  }

  public String getFirstName() {
    return firstName;
  }

  public void setFirstName(String firstName) {
    this.firstName = firstName;
  }

  public String getLastName() {
    return lastName;
  }

  public void setLastName(String lastName) {
    this.lastName = lastName;
  }
}
