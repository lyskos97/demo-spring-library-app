package com.example.habrdemo;

public class Logger {
  private static Logger instance;
  private String logs = "";

  private Logger() { }

  public static synchronized Logger getInstance() {
    if (instance == null) {
      instance = new Logger();
    }

    return instance;
  }

  public void appendToLogs(String message) {
    logs +=  message + "\n";
  }

  public String getLogs() {
    return logs;
  }
}
