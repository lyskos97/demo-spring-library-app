package com.example.habrdemo.controllers;

import com.example.habrdemo.models.Book;
import com.example.habrdemo.repositories.BookRepository;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.servlet.ModelAndView;

import java.util.HashMap;
import java.util.Map;

@Controller
@RequestMapping("/books")
public class BookController {
  private BookRepository bookRepository;

  public BookController(BookRepository bookRepository) {
    this.bookRepository = bookRepository;
  }

  @GetMapping({"/", ""})
  public ModelAndView getBooks() {
    Map<String, Iterable<Book>> model = new HashMap<>();

    model.put("books", bookRepository.findAll());

    return new ModelAndView("books", model);
  }
}
