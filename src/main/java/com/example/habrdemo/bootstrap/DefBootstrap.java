package com.example.habrdemo.bootstrap;

import com.example.habrdemo.Logger;
import com.example.habrdemo.models.Author;
import com.example.habrdemo.models.Book;
import com.example.habrdemo.models.Publisher;
import com.example.habrdemo.repositories.AuthorRepository;
import com.example.habrdemo.repositories.BookRepository;
import com.example.habrdemo.repositories.PublisherRepository;
import org.springframework.context.ApplicationListener;
import org.springframework.context.event.ContextRefreshedEvent;
import org.springframework.stereotype.Component;

@Component
public class DefBootstrap implements ApplicationListener<ContextRefreshedEvent> {
  private final BookRepository bookRepository;
  private final AuthorRepository authorRepository;
  private final PublisherRepository publisherRepository;

  public DefBootstrap(BookRepository bookRepository, AuthorRepository authorRepository, PublisherRepository publisherRepository) {
    this.bookRepository = bookRepository;
    this.authorRepository = authorRepository;
    this.publisherRepository = publisherRepository;
  }

  @Override
  public void onApplicationEvent(ContextRefreshedEvent contextRefreshedEvent) {
    initData();
  }

  private void initData() {
    Author author = new Author("Erich", "Evans");
    Publisher publisher = new Publisher("O'Rilley", "London, Fleet st. 21");
    Book book = new Book("DDD", "0000-ABCD");

    Author author1 = new Author("Rod", "Johnson");
    Book book1 = new Book("J2EE, Development without EGB", "2222-EFGH");
    Publisher publisher1 = new Publisher("WROX", "Cambridge, Baker st. 143");

    book.setAuthor(author);
    book.setPublisher(publisher);

    this.authorRepository.save(author);
    this.publisherRepository.save(publisher);
    this.bookRepository.save(book);

    book1.setAuthor(author1);
    book1.setPublisher(publisher1);

    this.authorRepository.save(author1);
    this.publisherRepository.save(publisher1);
    this.bookRepository.save(book1);
    System.out.println(Logger.getInstance().getLogs());
  }
}
