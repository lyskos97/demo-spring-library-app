package com.example.habrdemo;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class HabrdemoApplication {

	public static void main(String[] args) {
		SpringApplication.run(HabrdemoApplication.class, args);
	}
}
